import React from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import {createStackNavigator} from 'react-navigation'

import HomeScreen from './components/HomeScreen.js';
import MarsScreen from './components/MarsScreen.js';
import TimerScreen from './components/TimerScreen.js';
import LiveScreen from './components/LiveScreen.js';

import { YellowBox } from 'react-native'
import MissionScreen from './components/MissionScreen.js';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated'])
YellowBox.ignoreWarnings(['Warning: Can\'t call setState (or forceUpdate)']);




export default class App extends React.Component{
  render() {
    return (
      
      <ImageBackground
  source={require('./assets/background.jpg')}
  style={{width: '100%', height: '100%'}}
> 
<RootStack style={{backgroundColor:'#FFFFFF'}} />
</ImageBackground>
    );
  }
  static navigationOptions = {
    title: 'Chat',
    headerStyle: { backgroundColor: 'red' },
    headerTitleStyle: { color: 'green' },
  }
}



const RootStack = createStackNavigator({

  Home: {screen: HomeScreen},
  Mars: {screen: MarsScreen},
  Timer: {screen: TimerScreen},
  Live: {screen: LiveScreen},
  Mission: {screen : MissionScreen}
},
{
  initialRouteName: 'Home',
  cardStyle: {
  backgroundColor: 'transparent',
  },
}
);
