import React from 'react';
import { StyleSheet, Text, View,Animated, Easing, WebView} from 'react-native';
import {Button} from 'react-native-elements';
import Video from 'react-native-video'

export default class LiveScreen extends React.Component{
    render(){
        return(
            <WebView
            style={{width:360, height: 320, justifyContent:'center', alignItems:'center'}}
            javaScriptEnabled={true}
            source={{uri: 'https://youtu.be/wwMDvPCGeE0'}}
    />
        )
    }
}