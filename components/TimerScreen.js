import React from 'react';
import { StyleSheet, Text, View,Animated, Easing} from 'react-native';
import {Button} from 'react-native-elements';


export default class TimerScreen extends React.Component{
    static navigationOptions = {
        headerStyle: { backgroundColor: 'transparent' },
        headerTitleStyle: { color: 'white' },
        headerTintColor: 'white'

            
      }
      state = {
        timer: null,
        seconds: 3104300
      };

      componentDidMount() {
        let sec = 0;
        let timer = setInterval(
            () => {
                this.setState({
                    seconds: this.state.seconds-1
                })
            }, 1000);
        this.setState({timer});
      }
     

    render(){
        mseconds = parseInt(this.state.seconds%60);
        minutes= parseInt(this.state.seconds/60);
        mminutes = parseInt(minutes%60);
        hours = parseInt(this.state.seconds/(60*60));
        mhours = parseInt(hours%24);
        days = parseInt(this.state.seconds/(60*60*24));
        return(
            <View style={{justifyContent:'center',alignItems:'center', flexDirection:'row', flexWrap:'wrap', marginTop: 220}} >
                
                    <Text style={{color:'black',borderColor:'black',borderWidth:2 ,textAlign:'center', fontSize: 45, fontWeight: '700', width: 80, backgroundColor:'#fcf470', borderRadius: 4, marginVertical: 5}} >{days}</Text>
                
                    <Text style={{color:'black',borderColor:'black',borderWidth:2,textAlign:'center', fontSize: 45, fontWeight: '700', width: 80,  backgroundColor:'#fcf470', borderRadius: 4}} > {mhours} </Text>
                
                    <Text style={{color:'black',borderColor:'black',borderWidth:2,textAlign:'center', fontSize: 45, fontWeight: '700', width: 80,  backgroundColor:'#fcf470', borderRadius: 4}} >{mminutes}</Text>
                
                    <Text style={{color:'black',borderColor:'black',borderWidth:2,textAlign:'center', fontSize: 45, fontWeight: '700',  width: 80, backgroundColor:'#fcf470', borderRadius: 4}} >{mseconds}</Text>
                
            </View>
        )
    }
}   