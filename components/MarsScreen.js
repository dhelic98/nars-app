import React from 'react';
import { StyleSheet, Text, View,Animated, Easing, WebView} from 'react-native';
import {Button} from 'react-native-elements';


export default class MarsScreen extends React.Component{
    static navigationOptions = {
        headerStyle: { backgroundColor: 'transparent' },
        headerTitleStyle: { color: 'white' },
        headerTintColor: 'white'

            
      }
    render(){
        return(
            <View>
                <Text
                   style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:30, fontSize: 40, color:'white'}}
                >How to Martian</Text>
                <Text
                   style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:25, fontSize: 22, color:'white'}}
                >Orbital characteristics</Text>
                <Text
                   style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:8, fontSize: 18, color:'white'}}
                >Epoch J2000</Text>
                <View style={{justifyContent:'center', alignItems:'center', marginTop: 10}}>
                <Text
                   style={{ fontSize: 16, color:'white'}}
                >Aphelion 249200000 km</Text>
                <Text
                   style={{ fontSize: 16, color:'white'}}
                >Perihelion 206700000 km</Text>
                <Text
                   style={{ fontSize: 16, color:'white'}}
                >Semi-major axis 227939200 km</Text>
                 <Text
                   style={{ fontSize: 14, color:'white', marginHorizontal: 30, marginTop: 20}}
                >Mars is the fourth planet from the Sun and the second-smallest planet in the Solar System after Mercury. In English, Mars carries a name of the Roman god of war, and is often referred to as the "Red Planet" because the reddish iron oxide prevalent on its surface gives it a reddish appearance that is distinctive among the astronomical bodies visible to the naked eye. Mars is a terrestrial planet with a thin atmosphere, having surface features reminiscent both of the impact craters of the Moon and the valleys, deserts, and polar ice caps of Earth.</Text>
                </View>
            </View>
        );
    }
}