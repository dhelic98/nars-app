import React from 'react';
import { StyleSheet, Text, View,Animated, Easing, Image, StatusBar} from 'react-native';
import {Button} from 'react-native-elements';


export default class HomeScreen extends React.Component{
    static navigationOptions = {
        headerStyle: { backgroundColor: 'transparent' },
        headerTitleStyle: { color: 'white' },
      }
    render(){
        const {navigate} =this.props.navigation;
        return(
            
            <View style={{backgroundColor: 'transparent'}}>
            <StatusBar hidden />
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Image style={{width: 180, height: 180}} source={require('../assets/logo.png')}/>
                </View>
                <View
                style={{flex: 1, flexDirection: 'row', marginTop: 60, justifyContent: 'space-evenly', marginHorizontal: 20}}
                >
                <Button
                    title='Mars'
                    titleStyle={{
                        fontSize: 22,
                        fontFamily: 'Cooper Black',
                        fontWeight: '700',
                        color:'black'
                    }}
                    buttonStyle={{
                        backgroundColor: "#fcf470",
                        width: 160,
                        height: 53,
                        borderColor: "black",
                        borderWidth: 3,
                        borderRadius: 5
                      }}
                      onPress={() =>
                        navigate('Mars')
                      }
                />
                <Button
                    title='Hologram'
                    titleStyle={{
                        fontSize: 22,
                        fontFamily: 'Cooper Black',
                        fontWeight: '700',
                        color:'black'
                    }}
                    buttonStyle={{
                        backgroundColor: "#fcf470",
                        width: 160,
                        height: 53,
                        borderColor: "black",
                        borderWidth: 3,
                        borderRadius: 5
                      }}
                      onPress={
                      ()=>
                       navigate('Timer')
                      }
                />
                </View>
                <View style={{flex: 1, flexDirection: 'row', marginTop: 60, justifyContent: 'space-evenly', marginHorizontal: 20}}>
                <Button
                    title='Live'
                    titleStyle={{
                        fontSize: 22,
                        fontFamily: 'Cooper Black',
                        fontWeight: '700',
                        color:'black'
                    }}
                    buttonStyle={{
                        backgroundColor: "#fcf470",
                        width: 160,
                        height: 53,
                        borderColor: "black",
                        borderWidth: 3,
                        borderRadius: 5
                      }}
                      onPress={() =>
                        navigate('Live')
                      }
                />
                <Button
                    title='Mission'
                    titleStyle={{
                        fontSize: 22,
                        fontFamily: 'Cooper Black',
                        fontWeight: '700',
                        color:'black'
                    }}
                    buttonStyle={{
                        backgroundColor: "#fcf470",
                        width: 160,
                        height: 53,
                        borderColor: "black",
                        borderWidth: 3,
                        borderRadius: 5,
                        
                      }}
                      
                      onPress={
                        ()=>
                         navigate('Mission')
                        }
                />
                </View>
                <View
                 style={{justifyContent: 'center', alignItems: 'center'}}
                >
                <Button
                    title='Timer'
                    titleStyle={{
                        fontSize: 22,
                        fontFamily: 'Cooper Black',
                        fontWeight: '700',
                        color:'black'
                    }}
                    buttonStyle={{
                        backgroundColor: "#fcf470",
                        width: 320,
                        height: 53,
                        borderColor: "black",
                        borderWidth: 3,
                        borderRadius: 5,
                        marginTop: 80
                      }}
                      onPress={() =>
                        navigate('Timer')
                      }
                />
                </View>
            </View>
        );
    }
}