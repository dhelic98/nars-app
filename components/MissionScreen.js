import React from 'react';
import { StyleSheet, Text, View,Animated, Easing, ScrollView} from 'react-native';
import {Button} from 'react-native-elements';


export default class MissionScreen extends React.Component{
    static navigationOptions = {
        headerStyle: { backgroundColor: 'transparent' },
        headerTitleStyle: { color: 'white' },
        headerTintColor: 'white'

            
      }
    render(){
        return(
            <ScrollView>
                 <Text
                   style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 40, color:'white'}}
                >Active Missions</Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:25, fontSize: 22, color:'white'}}
                >
                ExoMars Trace Gas Orbiter
                </Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 20, color:'white'}}
                >
                Mars orbiter and lander (ESA)
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:15, fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Launch: March 14, 2016
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700', fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Mars orbit insertion: October 19, 2016
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:10, fontSize: 16, color:'white', marginHorizontal: 25}}
                >
                This first mission of ESA's ExoMars program consists of a Trace Gas Orbiter plus an Entry, descent and landing Demonstrator Module, known as Schiaparelli (which transmitted data during its descent before crash landing on the martian surface). The main objectives of this mission are to search for evidence of methane and other trace atmospheric gases that could be signatures of active biological or geological processes and to test key technologies in preparation for ESA's contribution to subsequent missions to Mars.
                </Text>


                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:35, fontSize: 22, color:'white'}}
                >
                MAVEN
                </Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 20, color:'white'}}
                >
                Mars orbiter (NASA)
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:15, fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Launch: November 18, 2013
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700', fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Mars orbit insertion: September 22, 2014
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:10, fontSize: 16, color:'white', marginHorizontal: 25}}
                >
                MAVEN, which stands for Mars Atmosphere and Volatile Evolution mission, has provided first-of-its-kind measurements to address key questions about Mars climate and habitability and improve understanding of dynamic processes in the upper Martian atmosphere and ionosphere.
                </Text>



                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:35, fontSize: 22, color:'white'}}
                >
                Mars Orbiter Mission (MOM)
                </Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 20, color:'white'}}
                >
                Mars orbiter (ISRO)
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:15, fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Launch: November 5, 2013
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700', fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Mars orbit insertion: September 24, 2014
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:10, fontSize: 16, color:'white', marginHorizontal: 25}}
                >
                Sometimes referred to by the nickname "Mangalyaan," the Mars Orbiter Mission is India's first interplanetary spacecraft. It is primarily a technology demonstration mission that carries a small, 15-kilogram payload of 5 science instruments. It entered orbit at Mars in September 2014, just two days after the arrival of NASA's MAVEN mission. The orbit is highly elliptical, from 387 to 80,000 kilometers.
                </Text>



                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:35, fontSize: 22, color:'white', marginHorizontal: 25}}
                >
                Curiosity (Mars Science Laboratory) (MSL)
                </Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 20, color:'white'}}
                >
                Mars rover (NASA)
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:15, fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Launch: November 26, 2011 
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700', fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Landing: August 6, 2012
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:10, fontSize: 16, color:'white', marginHorizontal: 25}}
                >
                Curiosity is the next generation of rover, building on the successes of Spirit and Opportunity. It landed in Gale Crater, the location of a 5+ km tall mound of layered sedimentary material, which Curiosity has found was at least partially deposited in a lake setting. The rover has also made key discoveries such as the detection of organic material. After a 2-(Earth)-year trek from its landing site, it is now entering the foothills of the mound, dubbed "Mount Sharp" (or Aeolis Mons), where it will then start its ascent up the mound.
                </Text>



                  <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:35, fontSize: 22, color:'white', marginHorizontal: 25}}
                >
                Mars Reconnaissance Orbiter
                </Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 20, color:'white'}}
                >
                In orbit at Mars (NASA)
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:15, fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Launch: August 12, 2005
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700', fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Mars arrival: March 10, 2006
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:10, fontSize: 16, color:'white', marginHorizontal: 25}}
                >
                The Mars Reconnaissance Orbiter is searching for evidence of past water on Mars, using the most powerful camera and spectrometer ever sent to Mars. Its cameras are also helping in the search for landing sites for future Mars rovers and landers, and to monitor martian weather on a day-to-day basis.
                </Text>
                


                 <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:35, fontSize: 22, color:'white', marginHorizontal: 25}}
                >
                Mars Exploration Rover Opportunity
                </Text>
                <Text
                style={{justifyContent:'center', textAlign:'center',fontWeight:'700',marginTop:15, fontSize: 20, color:'white'}}
                >
                Currently roving across Mars (NASA)
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:15, fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Launch: July 7, 2003
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700', fontSize: 17, color:'white', marginHorizontal: 25}}
                >
                Landing: January 24, 2004
                </Text>
                <Text
                style={{justifyContent:'center',fontWeight:'700',marginTop:10, fontSize: 16, color:'white', marginHorizontal: 25}}
                >
                Opportunity landed in Meridiani Planum at 354.4742°E, 1.9483°S, immediately finding the hematite mineral that had been seen from space by Mars Global Surveyor. After roving more than 33 kilometers, Opportunity arrived at the 22-kilometer-diameter crater Endeavour, a target it is currently exploring.
                </Text>



            </ScrollView>
        )
    }
}